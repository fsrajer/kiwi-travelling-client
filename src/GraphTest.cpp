#include <fstream>
#include <cstdio>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "Graph.h"

TEST_CASE("read test") {

  Graph g;
  g.readData("c:/workspace/kiwi-travelling-client/data_5.txt");

  /*CHECK(g.getCost(0, 1, 0) == 665);
  CHECK(g.getCost(2, 4, 2) == 265);
  CHECK(g.getCost(4, 0, 3) == 865);
  CHECK(g.getCost(0, 4, 0) == Graph::NO_EDGE);*/
}

TEST_CASE("super easy search test") {

  Graph g;
  g.readData("c:/workspace/kiwi-travelling-client/data_3.txt");

  vector<char> route;
  g.findBestRoute('A', &route);
  CHECK(route[0] == 'A');
  CHECK(route[1] == 'C');
  CHECK(route[2] == 'B');
  CHECK(route[3] == 'A');
}

TEST_CASE("easy search test") {

  Graph g;
  g.readData("c:/workspace/kiwi-travelling-client/data_5.txt");

  vector<char> route;
  int cost = g.findBestRoute('C', &route);
  CHECK(cost == 1850);
  CHECK(route[0] == 'C');
  CHECK(route[1] == 'B');
  CHECK(route[2] == 'E');
  CHECK(route[3] == 'D');
  CHECK(route[4] == 'A');
  CHECK(route[5] == 'C');
}