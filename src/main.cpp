#include <iostream>
#include <string>
#include <chrono>

#include "Graph.h"

using std::cout;
using std::string;

void printUsage() 
{
  cout << "Usage: TravellingClient data_filename city_of_origin\n";
}

int main(int argc, char *argv[]) 
{  
  if (argc != 3){
    printUsage();
    return EXIT_FAILURE;
  }

  string filename(argv[1]);
  char originCity = argv[2][0];

  Graph g;

  try {
    g.readData(filename);
  }
  catch(std::exception e){
    cout << e.what() << "\n";
    printUsage();
    return EXIT_FAILURE;
  }

  auto begin = std::chrono::high_resolution_clock::now();
  vector<char> route;
  int cost = g.findBestRoute(originCity, &route);
  auto end = std::chrono::high_resolution_clock::now();

  cout << "Total cost: " << cost << "\n";

  for (char city : route)
    cout << city << " - ";

  cout << "Runtime: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(
      end - begin).count() << "ms\n";
}