#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

class Graph {
public:
  Graph();
  void readData(const string& filename);
  int findBestRoute(char origin, vector<char> *route);

  static constexpr int NO_EDGE = -1;

private:
  const int& edgeCost(int from, int to, int day) const;
  int& edgeCost(int from, int to, int day);

  void findNumberOfCities(const string& filename);
  void findBestRouteImpl(int node, int cost);


  /// [from*N*N + to*N + day], NO_EDGE for undefined edges
  vector<int> graph_;
  int nCities_;
  int minEdgeCost_;
  
  // work variables for the recursion
  int searchOrigin_; 
  vector<bool> visited_;
  vector<int> currRoute_;
  int bestCost_;
  vector<int> bestRoute_;
};




