#include "Graph.h"

#include <fstream>
#include <exception>
#include <limits>
#include <iostream>

Graph::Graph()
  : nCities_(0), graph_(0)
{
}

void Graph::readData(const string& filename)
{
  findNumberOfCities(filename);

  graph_.resize(nCities_*nCities_*nCities_, NO_EDGE);

  std::ifstream file(filename);

  if (!file.is_open()) {
    throw std::runtime_error("Could not open file.");
  }

  minEdgeCost_ = std::numeric_limits<int>::max();
  string line;
  while (!file.eof()) {
    char fromName, toName;
    int day, cost;
    file >> fromName >> toName >> day >> cost;
    std::getline(file, line);

    // make zero based
    int from = fromName - 'A';
    int to = toName - 'A';
    day--;

    edgeCost(from, to, day) = cost;
    if (cost < minEdgeCost_)
      minEdgeCost_ = cost;
  }
}

const int& Graph::edgeCost(int from, int to, int day) const
{
  return graph_[from * nCities_ * nCities_ + to * nCities_ + day];
}

int& Graph::edgeCost(int from, int to, int day)
{
  return graph_[from * nCities_ * nCities_ + to * nCities_ + day];
}

int Graph::findBestRoute(char origin, vector<char> *route)
{
  vector<int> routeIdxs;

  searchOrigin_ = origin - 'A';
  visited_.resize(nCities_, false);
  currRoute_.clear();
  currRoute_.reserve(nCities_ + 1);
  bestCost_ = std::numeric_limits<int>::max();
  bestRoute_.clear();
  bestRoute_.reserve(nCities_ + 1);

  findBestRouteImpl(searchOrigin_, 0);

  for (int cityIdx : bestRoute_)
    route->push_back(static_cast<char>(cityIdx) + 'A');

  return bestCost_;
}

void Graph::findNumberOfCities(const string& filename)
{
  std::ifstream file(filename);

  if (!file.is_open()) {
    throw std::runtime_error("Could not open file.");
  }

  string line;
  while (!file.eof()) {
    char fromName, toName;
    int day, cost;
    file >> fromName >> toName >> day >> cost;
    std::getline(file, line);

    int from = fromName - 'A';
    int to = toName - 'A';
    
    if (from > nCities_-1)
      nCities_ = from+1;
    if (to > nCities_-1)
      nCities_ = to+1;
  }
}

void Graph::findBestRouteImpl(int node, int cost)
{
  int day = static_cast<int>(currRoute_.size());

  // existing path cost + heuristic
  if ((cost + (nCities_ - day) * minEdgeCost_) > bestCost_)
    return;


  currRoute_.push_back(node);
  visited_[node] = true;

  if (currRoute_.size() == nCities_) {
    int finalEdgeCost = edgeCost(node, searchOrigin_, day);
    cost += finalEdgeCost;
    if (finalEdgeCost != NO_EDGE && cost < bestCost_) {
      bestCost_ = cost;
      bestRoute_ = currRoute_;
      bestRoute_.push_back(searchOrigin_);
    }
  }
  else {
    for (int to = 0; to < nCities_; to++)
    {
      if (!visited_[to] && edgeCost(node, to, day) != NO_EDGE) {
        findBestRouteImpl(to, cost + edgeCost(node, to, day));
      }
    }
  }

  currRoute_.pop_back();
  visited_[node] = false;
}