# Travelling client problem

Description here: https://docs.google.com/document/d/1gEi9YAOdVuq8NAv3LvBegAA4vPzolVQDNIczew2rf4I/edit

## Functionality
### Input
The application will receive two command line parameters. The first parameter is a path to a file, while the second parameter is the letter denoting the origin.

### Output
Print the output to stdout the total cost, path and runtime.

## Compatibility
The code is written using cmake so you will need cmake to build your solution. 